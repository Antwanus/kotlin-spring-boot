package main;

import com.av.Customer;
import com.av.CustomerDatabase;

public class Main {
   public static void main(String[] args) {
      CustomerDatabase db = new CustomerDatabase();
      for (Customer c : db.getImmutableCustomersList()) {
         System.out.println(c);
      }
      try {
         db.addCustomer(new Customer(20L, "hi"));
      } catch (IllegalAccessException e) {
         e.printStackTrace();
      }
   }
}
