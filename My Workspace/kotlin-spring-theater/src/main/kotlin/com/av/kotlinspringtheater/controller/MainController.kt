package com.av.kotlinspringtheater.controller

import com.av.kotlinspringtheater.data.PerformanceRepository
import com.av.kotlinspringtheater.data.SeatRepository
import com.av.kotlinspringtheater.domain.Booking
import com.av.kotlinspringtheater.domain.Performance
import com.av.kotlinspringtheater.domain.Seat
import com.av.kotlinspringtheater.service.BookingService
import com.av.kotlinspringtheater.service.TheaterService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.ModelAndView

@RestController
class MainController {
    //lateinit tells kotlin, don't worry about initializing this value, this will be handles by some1 else
    //-> whenever we do dependency injection in Spring, we must provide 'lateinit'
    @Autowired  lateinit var theaterService:TheaterService
    @Autowired  lateinit var bookingService:BookingService
    @Autowired  lateinit var seatRepository:SeatRepository
    @Autowired  lateinit var performanceRepository:PerformanceRepository

    @RequestMapping("")
    fun homePage(): ModelAndView {
        val model = mapOf(
            "bean" to CheckAvailabilityBackingBean(),
            "performances" to performanceRepository.findAll(),
            "seatNums" to 1..36,
            "seatRows" to 'A'..'O'

        )
        return ModelAndView("seatBooking",model)
    }

    @PostMapping("booking")
    fun bookASeat(bean: CheckAvailabilityBackingBean) : ModelAndView {
        val booking = bookingService.reserveSeat(bean.seat!!, bean.performance!!, bean.customerName)
        return ModelAndView("bookingConfirmed", "booking", booking)
    }

    @PostMapping("checkAvailability")
    fun checkAvailability(bean: CheckAvailabilityBackingBean):ModelAndView {
        val selectedSeat:Seat = bookingService.findSeat(bean.selectedSeatNum, bean.selectedSeatRow)!!
        val selectedPerformance = performanceRepository.findById(bean.selectedPerformance!!).get()
        bean.seat = selectedSeat
        bean.performance = selectedPerformance

        val result = bookingService.isSeatFree(selectedSeat, selectedPerformance)
        bean.available = result
        if (!result) {
            bean.booking = bookingService.findBooking(selectedSeat, selectedPerformance)
        }
        val model = mapOf ("bean" to bean,
            "performances" to performanceRepository.findAll(),
            "seatNums" to 1..36,
            "seatRows" to 'A'..'O')

        return ModelAndView("seatBooking", model)
    }

    @RequestMapping("bootstrap")
    fun bootstrapDataAndSaveToH2DB():ModelAndView{
        val seats = theaterService.seats
        seatRepository.saveAll(seats)
        return homePage()
    }

}
class CheckAvailabilityBackingBean() {
    var selectedSeatNum:Int = 1
    var selectedSeatRow:Char = 'A'
    var selectedPerformance:Long? = null
    var customerName:String = ""

    var available :Boolean? = null
    var seat:Seat? = null
    var performance:Performance? = null
    var booking:Booking? = null


}

