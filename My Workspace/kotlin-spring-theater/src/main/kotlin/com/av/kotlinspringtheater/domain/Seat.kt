package com.av.kotlinspringtheater.domain

import java.math.BigDecimal
import javax.persistence.*

/** Hibernate uses the default constructor of a class/entity & uses the setters when creating an object of the record
 *  ->  Instead of refactoring, Kotlin has a compiler plugin for this problem
 *      ->  The plugin allows Hibernate (& only Hibernate) to create a seat (default without params)
 *          & use setters on each of the val
 */
@Entity
data class Seat(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "seat_id")
    val id:Long,
    @Column(name = "seat_row")
    val row:Char,
    @Column(name = "seat_num")
    val num:Int,
    @Column(name = "seat_price")
    val price:BigDecimal,
    @Column(name = "seat_description")
    val description:String
) {
    override fun toString(): String = "Seat $row-$num $$price ($description)"
}