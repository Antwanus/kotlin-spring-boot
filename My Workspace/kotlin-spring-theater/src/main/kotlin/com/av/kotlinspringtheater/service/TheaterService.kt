package com.av.kotlinspringtheater.service

import com.av.kotlinspringtheater.domain.Seat
import org.springframework.stereotype.Service
import java.math.BigDecimal

@Service
class TheaterService {
	val seats                                       // immutable list
        get() = hiddenSeats.toList()                // get() returns new immutableList from the private (final) mutableList
    private val hiddenSeats = mutableListOf<Seat>() // private mutable list

    init {
        fun getPrice(row: Int, num: Int) : BigDecimal {
            return when {
                row >=14 -> BigDecimal(14.50)
                num <=3 || num >= 34 -> BigDecimal(16.50)
                row == 1 -> BigDecimal(21)
                else -> BigDecimal(18)
            }
        }
        fun getDescription(row: Int, num: Int) : String {
            return when {
                row == 15 -> "Back Row"
                row == 14 -> "Cheaper Seat"
                num <=3 || num >= 34 -> "Restricted View"
                row <=2 -> "Best View"
                else -> "Standard Seat"
            }
        }
        for (row in 1..15) {
            for (num in 1..36) {
                // converting a number to a char by adding 64 to it & converting it into a Char
                hiddenSeats.add( Seat(0L, (row+64).toChar(), num, getPrice(row,num), getDescription(row,num) ))
            }
        }
    }

    fun find(seatNum:Int, row:Char):Seat =
        seats.first { e -> e.row == row && e.num == seatNum }   // == seats.filter { predicate }.first()


}
