package com.av.kotlinspringtheater.controller

import com.av.kotlinspringtheater.service.ReportingService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.ModelAndView
import javax.websocket.server.PathParam
import kotlin.reflect.full.declaredMemberFunctions
import kotlin.reflect.full.memberFunctions

@RestController
@RequestMapping("reports")
class ReportsController {
    @Autowired
    lateinit var reportingService:ReportingService

    private fun getListOfReports() = reportingService::class.declaredMemberFunctions.map { it.name }

    @GetMapping("")
    fun main() = ModelAndView("reports", mapOf("reports" to getListOfReports()))

    @GetMapping("getReport")
    fun getReport(@PathParam("report") report:String):ModelAndView {
        val matchedReport = reportingService::class.declaredMemberFunctions.firstOrNull { it.name == report }
        val result = matchedReport?.call(reportingService) ?: ""
        return ModelAndView("reports", mapOf(   "reports" to getListOfReports(),
                                                          "result" to result
        ))
    }
}