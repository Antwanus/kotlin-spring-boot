package com.av.kotlinspringtheater.service

import com.av.kotlinspringtheater.data.BookingRepository
import com.av.kotlinspringtheater.data.SeatRepository
import com.av.kotlinspringtheater.domain.Booking
import com.av.kotlinspringtheater.domain.Performance
import com.av.kotlinspringtheater.domain.Seat
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class BookingService {
    @Autowired
    lateinit var bookingRepo:BookingRepository
    @Autowired
    lateinit var seatRepo:SeatRepository

    fun isSeatFree(seat:Seat, performance:Performance):Boolean {
        val matchedBookings = findBooking(seat, performance)
        return matchedBookings == null
    }

    fun findSeat(seatNum:Int, seatRow:Char):Seat? {
        return seatRepo.findAll()
            .firstOrNull { seat -> seat.num == seatNum && seat.row == seatRow }
    }
    fun findBooking(seat: Seat, performance: Performance) : Booking? {
        val bookings = bookingRepo.findAll()
        val matchedBookings = bookings.filter { it.seat == seat && it.performance == performance }
        return matchedBookings.firstOrNull()
    }
    fun reserveSeat(seat: Seat, performance: Performance, customerName: String) : Booking {
        val booking = Booking(0,customerName)
        booking.seat = seat
        booking.performance = performance

        bookingRepo.save(booking)
        return booking
    }
}