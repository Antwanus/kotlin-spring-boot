package com.av.kotlinspringtheater.domain

import javax.persistence.*

@Entity
data class Performance (
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id:Long,
//    @OneToMany(mappedBy = "performance")      //  This is a bad idea because .equals() & .hashCode() are generated
//    val bookings:List<Booking>,               //  from the default constructor (=> highly inefficient)
    val title:String
) {
    @OneToMany(mappedBy = "performance")
    lateinit var bookings:List<Booking>         // will be initialized by hibernate, hence lateinit
}