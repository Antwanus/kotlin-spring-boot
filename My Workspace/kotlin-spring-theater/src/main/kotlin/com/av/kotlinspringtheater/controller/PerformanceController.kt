package com.av.kotlinspringtheater.controller

import com.av.kotlinspringtheater.data.PerformanceRepository
import com.av.kotlinspringtheater.domain.Performance
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.ModelAndView

@RestController
@RequestMapping("performances")
class PerformanceController {

    @Autowired
    lateinit var performanceRepository: PerformanceRepository

    @RequestMapping("")
    fun performancesHomePage() =
        ModelAndView("performances/home","performances", performanceRepository.findAll())

    @RequestMapping("add")
    fun addPerformance() =
        ModelAndView("performances/add","performance", Performance(0,""))

    @PostMapping("save")
    fun savePerformance(performance: Performance):ModelAndView {
        performanceRepository.save(performance)
        return performancesHomePage()
    }

}