package com.av.kotlinspringtheater

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KotlinSpringTheaterApplication

fun main(args: Array<String>) {
	runApplication<KotlinSpringTheaterApplication>(*args)
}
