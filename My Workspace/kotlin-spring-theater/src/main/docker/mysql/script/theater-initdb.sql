DROP DATABASE IF EXISTS theater;
DROP USER IF EXISTS `theater_service`@`%`;
CREATE DATABASE IF NOT EXISTS theater CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER IF NOT EXISTS `theater_service`@`%` IDENTIFIED WITH mysql_native_password BY 'password';
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, REFERENCES, INDEX, ALTER, EXECUTE, CREATE VIEW, SHOW VIEW,
CREATE ROUTINE, ALTER ROUTINE, EVENT, TRIGGER ON `theater`.* TO `theater_service`@`%`;
FLUSH PRIVILEGES;