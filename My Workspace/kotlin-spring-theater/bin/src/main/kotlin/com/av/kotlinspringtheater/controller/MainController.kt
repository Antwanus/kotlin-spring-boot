package com.av.kotlinspringtheater.controller

import com.av.kotlinspringtheater.data.SeatRepository
import com.av.kotlinspringtheater.domain.Seat
import com.av.kotlinspringtheater.service.BookingService
import com.av.kotlinspringtheater.service.TheaterService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.ModelAndView


@RestController
class MainController {
    //lateinit tells kotlin, don't worry about initializing this value, this will be handles by some1 else
    //-> whenever we do dependency injection in Spring, we must provide 'lateinit'
    @Autowired
    lateinit var theaterService:TheaterService
    @Autowired
    lateinit var bookingService:BookingService
    @Autowired
    lateinit var seatRepository:SeatRepository

    @RequestMapping("")
    fun homePage() = ModelAndView("seatBooking","bean",CheckAvailabilityBackingBean())

    @PostMapping("checkAvailability")
    fun checkAvailability(bean: CheckAvailabilityBackingBean):ModelAndView {
        val foundSeat:Seat = theaterService.find(bean.selectedSeatNum,bean.selectedSeatRow)
        val seatIsFree:Boolean = bookingService.isSeatFree(foundSeat)
        bean.result = "Seat $foundSeat is " +   if(seatIsFree) "available!" else "booked :("
        return ModelAndView("seatBooking", "bean", bean)
    }

    @RequestMapping("bootstrap")
    fun bootstrapDataAndSaveToH2DB():ModelAndView{
        val seats = theaterService.seats
        seatRepository.saveAll(seats)
        return homePage()
    }

}
class CheckAvailabilityBackingBean() {
    val seatNums = 1..36
    val seatRows = 'A'..'O'

    var selectedSeatNum:Int = 1
    var selectedSeatRow:Char = 'A'

    var result:String = ""

}