package com.av.kotlinspringtheater.data

import com.av.kotlinspringtheater.domain.Booking
import com.av.kotlinspringtheater.domain.Performance
import com.av.kotlinspringtheater.domain.Seat
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository     interface SeatRepository:JpaRepository<Seat, Long>
@Repository     interface PerformanceRepository:JpaRepository<Performance, Long>
@Repository     interface BookingRepository:JpaRepository<Booking, Long>