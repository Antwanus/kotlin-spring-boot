package com.av.kotlinspringtheater.domain

import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne

data class Booking (
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id:Long,
    val customerName:String
) {
    @ManyToOne
    lateinit var seat:Seat
    @ManyToOne
    lateinit var performance:Performance
}