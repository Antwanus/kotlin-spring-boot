package com.av.kotlinspringtheater.domain

import java.math.BigDecimal
import javax.persistence.*

/** Hibernate uses the default constructor of a class/entity & uses the setters when creating an object of the record
 *  ->  Instead of refactoring, Kotlin has a compiler plugin for this problem
 *      ->  The plugin allows Hibernate (& only Hibernate) to create a seat (default without params)
 *          & use setters on each of the val
 */
@Entity
data class Seat(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id:Long,
    val rowNumber:Char,
    val num:Int,
    val price:BigDecimal,
    val description:String
) {
    override fun toString(): String = "Seat $rowNumber-$num $$price ($description)"
}