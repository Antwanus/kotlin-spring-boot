                docker exec -it { mysql_container_id } sh

sh-4.2# mysql -utheater_service -p
    Enter password:
    Welcome to the MySQL monitor.  Commands end with ; or \g.
    Your MySQL connection id is 31
    Server version: 8.0.23 MySQL Community Server - GPL

    Copyright (c) 2000, 2021, Oracle and/or its affiliates.

    Oracle is a registered trademark of Oracle Corporation and/or its
    affiliates. Other names may be trademarks of their respective
    owners.

    Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> show databases;
    +--------------------+
    | Database           |
    +--------------------+
    | information_schema |
    | theaterservice     |
    +--------------------+
    2 rows in set (0.00 sec)

mysql> use theaterservice;
    Reading table information for completion of table and column names
    You can turn off this feature to get a quicker startup with -A

    Database changed
mysql> show tables;
    +--------------------------+
    | Tables_in_theaterservice |
    +--------------------------+
    | booking                  |
    | hibernate_sequence       |
    | performance              |
    | seat                     |
    +--------------------------+
    4 rows in set (0.00 sec)

mysql> select * from booking;
    +-----+---------------+----------------+--------------+
    | id  | customer_name | performance_id | seat_seat_id |
    +-----+---------------+----------------+--------------+
    | 542 | Pablo         |              1 |            2 |
    +-----+---------------+----------------+--------------+
    1 row in set (0.00 sec)

mysql> select * from performance;
    +----+-----------+
    | id | title     |
    +----+-----------+
    |  1 | For once  |
    +----+-----------+
    1 row in set (0.00 sec)

mysql> select * from seat;
    +---------+------------------+----------+------------+----------+
    | seat_id | seat_description | seat_num | seat_price | seat_row |
    +---------+------------------+----------+------------+----------+
    |       2 | Restricted View  |        1 |      16.50 | A        |
    |       3 | Restricted View  |        2 |      16.50 | A        |
    .....
    540 rows in set (0.00 sec)