//package com.av;
//
//import java.util.List;
//
///**   everything is pretty straight forward
// *    -  u have a couple of annotations on the kotlin side to make it interop
// *    -  u can't call top lvl functions from kotlin, these need to be nested in a class/interface   (-> this is how java works)
// * */
//public class Main {
//   public static void main(String[] args) {
//      // exploring the Kotlin-Customer-data-class
//      var c = new Customer(10, "null");
//      long id = c.getId();
//      long l = c.component1();
//      if (id == l)   System.out.println("BINGO");
//
//      for (var i = 0; i < 10; i++) {
//         Customer c2 = c.copy(c.getId()+1+i, "RANDOM_BOT");
//         System.out.println(c2);
//      }
//      System.out.println(id+" "+l );
//
//      //exploring the Kotlin-Data-Service-class (has an immutable list)
//      var db = new CustomerDatabase();
//      List<Customer> customerList = db.getImmutableCustomersList();
//
//      try {
//         customerList.add(c); // Exception in thread "main" java.lang.UnsupportedOperationException
//      } catch (UnsupportedOperationException e) {
//         System.out.println("u can't change this list although .add, .remove,.. are visible in Java");
//         System.out.println(e);
//      }
//
//      try {
//         db.addCustomer(c);
//      } catch (IllegalAccessException e) {               //DOES NOT COMPILE if Kotlin is missing @Throws(E::class)
//         System.out.println("U can't add a customer: "+e);
//      }
//
//      //ref static function from kotlin
//      CustomerDatabase.Companion.helloWorld();  //OR CustomerDB.helloWorld() => if u supplied @JvmStatic on the function
//   }
//}