package com.av

data class Customer(val id:Long, val name:String)

class CustomerDatabase() {
    val immutableCustomersList = listOf(    Customer(1, "one"),
                                            Customer(2, "two"),
                                            Customer(3, "three"),
                                            Customer(4, "four")
    )
    @Throws(IllegalAccessException::class)
    fun addCustomer(c:Customer) {
        throw IllegalAccessException("u can't add a customer :-) (don't worry this is correct)")
    }
    companion object {
        fun helloWorld() {
            println("hello world")
        }
    }


}