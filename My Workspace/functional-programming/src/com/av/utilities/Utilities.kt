package com.av.utilities

fun toSentenceCase(input:String):String =   input[0].uppercase() + input.substring(1)

fun main() {
    println(toSentenceCase("hi"))
}