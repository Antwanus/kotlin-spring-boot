package com.av.application

fun applyFunctionToString(input:String, myFunction:(String)->(String)):String {
    return myFunction(input)
}
fun main() {
//    val result = applyFunctionToString("hello", ::toSentenceCase)
    val result = applyFunctionToString("hello") {s-> s[0].uppercase() + s.substring(1)}
    println(result)

    val colors = listOf("red", "green", "blue", "black")
    val uppercaseColors =
//        colors.map { s:String -> s.uppercase() }
        colors.map { it.uppercase() }

    uppercaseColors.forEach(System.out::println)

    val colorsStartingWithB =
//        colors.filter { s:String -> s[0] == 'b' || s[0] == 'B'}
        colors.filter { it.startsWith('b') }

    colorsStartingWithB.forEach{ print("$it ") }
    println("\n----------------------------------")

    colors  .flatMap {
                if (it.startsWith('b')) listOf(it, it)
                else listOf(it)
            }
            .forEach(System.out::println)

    println( colors.reduce{ acc:String, s:String -> "$acc, $s"  })  // result becomes acc next iteration

    val colorsWordLengthList = colors.map { it.length }
    println(colorsWordLengthList)
    println("avg: "+colorsWordLengthList.average())
    println("sum: "+colorsWordLengthList.sum())
    println("count: "+colorsWordLengthList.count())
    
    println( colorsWordLengthList.fold(0) {acc:Int, i:Int -> acc+i })
    println( colorsWordLengthList.fold(-17) {acc:Int, i:Int -> acc+i })

    println( colorsWordLengthList.fold(0) {acc: Int, i: Int -> if (acc > i) acc else i })


    val myMap = mapOf(1 to "one", 2 to "two", 3 to "three")
    myMap.filter { (key, v)-> v.startsWith("t") }
        .forEach{ (key, v)-> println("$key : $v") } // -> unlike .fold, expecting single argument => need to put k,v in ()


}