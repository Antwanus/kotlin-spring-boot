package com.av.application;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class JavaExamples {

   /* utilities package */
   public static Function<String, String> toSentenceCase = s -> s.substring(0, 1).toUpperCase() + s.substring(1);

   public static String applyFunctionToString(String input, Function<String, String> function) {
      return function.apply(input);
   }

   public static void main(String[] args) {
      String result = applyFunctionToString("hello", toSentenceCase);
      System.out.println(result);

      List<String> colorsList = new ArrayList<>(
          Arrays.asList(
            "red", "blue", "green", "purple", "orange", "black"
      ));

      List<String> uppercaseColorsList = new ArrayList<>();

      for (String color : colorsList) {
         uppercaseColorsList.add(color.toUpperCase());
      }
      System.out.println(uppercaseColorsList);


   }
}
