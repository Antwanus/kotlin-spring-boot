package main

import java.math.BigDecimal

/** In Kotlin, all classes are final by default. if you want to extend a class: 'open class Foo { }'
 * */
interface BookingManager {
    val version:String                                          // this value must be implemented
    fun isSeatFree(seat:Seat) :Boolean
    fun reserveSeat(sea:Seat, customerId:Long) :Boolean

    fun systemStatus() = "all systems ago"                     // default value/implementation
}
open class BookingManagerImpl(authKey:String):BookingManager {       // ':'-> extends/implements depends on type class/interface
    override val version = "1.1"
    override fun isSeatFree(seat: Seat):Boolean = true
    override fun reserveSeat(sea: Seat, customerId: Long):Boolean = false
    init {
        if (authKey != "secret") throw UnauthorizedException()
    }
}
class AnotherBookingManagerImpl :BookingManagerImpl("secret"), java.io.Closeable {
    override val version: String
        get() = "2.0"
    fun howManyBookings() = 10

    override fun close() {
        println("hi")
    }
}
class UnauthorizedException : Throwable() { }

// by implementing this method, the String class now has the following function
fun String.toMySentenceCase():String {
    // this points to the object (of type String) the method will be called upon
    return this[0].uppercase() + this.substring(1)
}

fun main() {
    val manager:BookingManager = BookingManagerImpl("secret")
    val isSeatFree = manager.isSeatFree(Seat(1, 1, BigDecimal(1), ""))
    println("isSeatFree= $isSeatFree")
    println(manager.systemStatus())

    val anotherManager = AnotherBookingManagerImpl()

    print(anotherManager.howManyBookings())
    print(" ")
    print("potatoes".toMySentenceCase())
}
