package main

import java.math.BigDecimal

// CLASS CAN NOT BE EDITED
data class MySeat(val row: Int, val num: Int, val price: BigDecimal, val description: String) {
    override fun toString(): String = "Seat $row-$num $$price ($description)"
}

class MyTheater {
    val seats:List<MySeat> = createListOfSeats()//THIS MUST BE AN IMMUTABLE LIST

    companion object {
        private const val SEATS_PER_ROW:Int = 36
        private const val NUMBER_OF_ROWS:Int = 15
        private val backRowPrice =  BigDecimal(14.5)                //ROW_15
        private val cheaperSeatPrice = BigDecimal(14.5)             //ROW_14
        private val restrictedViewSeatPrice = BigDecimal(16.5)      //ROW_{1..13}, SEAT_{1..3 && 34..36}
        private val standardSeatPrice = BigDecimal(18)              //ROW_{3..13}
        private val firstRowSeatPrice = BigDecimal(21)              //ROW_{1..2}

        fun createListOfSeats():List<MySeat> {
            val list = mutableListOf<MySeat>()
            for (rowNumber in 1..NUMBER_OF_ROWS) {
                if (rowNumber == 15)
                    for (seatNumber in 1..SEATS_PER_ROW) list.add(MySeat(rowNumber, seatNumber, backRowPrice, "Back Row"))

                if (rowNumber == 14)
                    for (seatNumber in 1..SEATS_PER_ROW) list.add(MySeat(rowNumber, seatNumber, cheaperSeatPrice, "Cheaper Seat"))

                if (rowNumber in 3..13) {
                    for (seatNumber in 1..SEATS_PER_ROW) {
                        if (seatNumber in 1..3 || seatNumber in 34..36)
                            list.add(MySeat(rowNumber, seatNumber, restrictedViewSeatPrice, "Restricted View"))
                        else
                            list.add(MySeat(rowNumber, seatNumber, standardSeatPrice, "Standard Seat"))
                }}
                if (rowNumber in 1..2) {
                    for (seatNumber in 1..SEATS_PER_ROW) {
                        if (seatNumber in 1..3 || seatNumber in 34..36)
                            list.add(MySeat(rowNumber, seatNumber, restrictedViewSeatPrice, "Restricted View"))
                        else
                            list.add(MySeat(rowNumber, seatNumber, firstRowSeatPrice, "First Row Seat"))
                }}
            }
            return list
        }
    }
}

fun main() {
    val cheapSeats = MyTheater().seats.filter { it.price == BigDecimal(16.5) }
    for (seat in cheapSeats) println(seat.toString())
}