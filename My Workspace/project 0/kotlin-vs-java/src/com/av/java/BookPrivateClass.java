package com.av.java;

/**   KOTLIN_EQUIVALENT= class Book(val id:Long, val title:String)       */
public class BookPrivateClass {
   private long id;
   private String title;

   public BookPrivateClass(long id, String title) {
      this.id = id;
      this.title = title;
   }
   public long getId() { return id; }
   public String getTitle() { return title; }
}
