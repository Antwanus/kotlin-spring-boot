package com.av.java;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class JavaExceptions {
/**
   public static double divide(int a, int b) throws InterruptedException {
      Thread.sleep(1000);
      return (double) a/b;
   }
*/
   private static void printFile() throws IOException {
      try (
          FileInputStream inputStream = new FileInputStream("file.txt")
      ) {
         int data = inputStream.read();
         while (data != -1) {
            System.out.println((char) data);
            data = inputStream.read();
         }
      } catch (FileNotFoundException e) {
         System.out.println("FILE NOT FOUND");
      }
   }
   public static void main(String[] args) throws IOException {
/**
      try {
         System.out.println(7/0);
      }
      catch (ArithmeticException e) {
         System.out.println("CAUGHT EXCEPTION: "+e);
      }
      try {
         Thread.sleep(1000);
      } catch (InterruptedException e) {
         e.printStackTrace();
      }
      try {
         System.out.println(divide(7, 0));
      } catch (InterruptedException e) {
         e.printStackTrace();
      }
*/

   JavaExceptions.printFile();

   }
}
