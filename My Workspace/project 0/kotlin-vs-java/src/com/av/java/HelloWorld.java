package com.av.java;

import java.math.BigDecimal;
import java.util.Random;

public class HelloWorld {
   public static void main(String[] args) {
   Object result;
   int randomInt = new Random().nextInt(3);

   if (randomInt == 1) {
      result = new BigDecimal(30);
   } else result = "howdy";

      System.out.println("result is currently => " +result);

      if (result instanceof BigDecimal) {
         // add 1_000_000
         result = ((BigDecimal) result).add( BigDecimal.valueOf(1_000_000));
      } else {
         // put it in uppercase
         String tmp = (String) result;
         result = tmp.toUpperCase();
      }

      System.out.printf("result is currently => " +result);

   }
}
