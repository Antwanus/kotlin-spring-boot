package com.av.sandbox.strategy;

public class Main { }
/**   the main gist is you can extract properties to an interface from the super class
 *    this makes it possible to have different implementations on that interfaces
 *    => loose coupling
 */
interface IFlyBehavior {
   default void fly() { System.out.print("I can fly"); }
}
class SimpleQuack implements IQuackBehavior {
   // implementation of different quacks...
}
interface IQuackBehavior {
   default void quack() { System.out.println("hi");}
}
interface IEatBehavior {
   default void eat() { System.out.println("This duck likes veg");}
}

abstract class Duck implements IFlyBehavior, IEatBehavior, IQuackBehavior {
   void display()  { System.out.println("This is a duck"); }
}
class CityDuck extends Duck {
   @Override
   public void eat() {
      System.out.println("This duck likes hot dogs");
   }
}



//abstract class Duck {
//   void quack()    { System.out.println("hi"); }
//   void display()  { System.out.println("This is a duck"); }
//   void fly()      { System.out.println("This boi can fly!"); }
//   void eat()      { }
//}
//class CityDuck extends Duck {
//   @Override public void eat()      { System.out.println("This duck likes hot dogs"); }
//   @Override public void quack()    { System.out.println("Bonjour"); }
//}
//class WildDuck extends Duck {
//   @Override public void eat()      { System.out.println("This duck likes veg"); }
//   @Override public void quack()    { System.out.println("Bonjour"); }
//}
//class RubberDuck extends Duck {
//   @Override public void quack() { }
//   @Override public void eat()   { }
//   @Override public void fly()   { }
//}
