package com.av.sandbox.singleton;

public class Singleton {
   private static Singleton instance;

   private Singleton() {   }
   public static Singleton getInstance() {
      if (instance == null) {
         instance = new Singleton();
      }
      return instance;
   }
}
class Main {
   public static void main(String[] args) {
      Singleton instance = Singleton.getInstance();
      Singleton instance2 = Singleton.getInstance();
      System.out.println(instance == instance2);
      System.out.println(instance.equals(instance2));
   }
}

