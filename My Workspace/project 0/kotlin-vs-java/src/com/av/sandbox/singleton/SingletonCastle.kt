package com.av.sandbox.singleton
/** - Kotlin’s representation of a Singleton class requires the object keyword only.
    - An object class can contain properties, functions and the init method.
    - The constructor method is NOT allowed.
    - An object cannot be instantiated in the way a class is instantiated.
    - An object gets instantiated when it is used for the first time providing lazy initialization.
    - Object declaration’s initialization is thread-safe. */
class SingletonCastle private constructor() {
    private object HOLDER {
        val INSTANCE = SingletonCastle()
    }
    companion object {
        val instance:SingletonCastle by lazy { HOLDER.INSTANCE }
    }
}

fun main() {
    val instance = SingletonCastle.instance
    val instance2 = SingletonCastle.instance
    println(instance === instance2)
    println(instance == instance2)
}

