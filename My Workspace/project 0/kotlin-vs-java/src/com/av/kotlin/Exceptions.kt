package com.av.kotlin

import java.io.FileInputStream
import java.io.FileNotFoundException
import java.lang.Exception

/* even when this annotation is set, kotlin doesn't need/warn anything
*   -> this is more for compatibility with Java (ie: calling this method from .java), where these signatures are needed
*/
@Throws(InterruptedException::class)
fun divide(a:Int, b:Int):Double {
    Thread.sleep(1000)
    return (a.toDouble() / b)

}

fun printFile() {
    println("printFile() has been called...")
    try {
        val inputStream = FileInputStream("file.txt")
        inputStream.use {
            //exception could be thrown here
            println(it)
        }
    } catch (e:FileNotFoundException) {
        println("file not found...")
    }
    println("printFile() is done...")
}

fun main() {
/*   INTRODUCTION
    try {

        println(7/0)
    } catch (e: ArithmeticException) {
        println("ERROR: $e")
    }
    /*  unlike Java, Kotlin does not want you to catch/throw InterruptedException..
     *  it gives no warning when you don't.. not even sonar(?)
     *
     *  the reason why Kotlin does not have checked exceptions, is because it's not really compatible with functional
     *      programming -> write code carefully & test thoroughly!
     */
     Thread.sleep(1000)
*/
    val result  =   try {
                        divide(5, 23)
                    }
                    catch (e:Exception) {
                        println(e)
                        0
                    }

    println(result)

    printFile()
}