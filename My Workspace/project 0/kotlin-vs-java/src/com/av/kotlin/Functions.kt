package com.av.kotlin

private fun printAString(s: String) {
    print(" $s")
}
private fun printAStringShorter(s: String) = print(" $s")
fun addTwoNumbers(one: Double, two: Double) :Double {
    return one + two
}
fun addTwoNumbersShorter(one:Double, two:Double):Double = one + two
fun addTwoNumbersShorterer(one:Double, two:Double) = one + two      //type is inferred on single expression functions
fun printSomeMath(
    one:Double = 1.1,   // can set a default value for a function parameter (no need to set it when called)
    two:Double = 1.1    // by setting both the function can be called with none, one or all params <3
) {
//    one = 2           // !COMPILE
    println("one - two = ${one - two}")
    println("one + two = ${one + two}")
    println("one * two = ${one * two}")
    fun innerFunction(s: String) = println(s)
    innerFunction("---------------------------- ")
}
fun methodTakesALambda(
    input:String,
//    action: java.util.function.Function<String, Int>
    action: (String)-> Int
) {
//    println(action.apply(input))
    println(action(input))
}


fun main() {
    printAString("xo")
    printAStringShorter("xo")
    print("\n" +addTwoNumbers(1.0, 2.2))
    print(" " +addTwoNumbersShorter(1.0, 2.2))
    print(" " +addTwoNumbersShorterer(1.0, 2.2))
    println()

    printSomeMath(100.1, 2.2)
    printSomeMath(two = 100.1, one= 2.2)
    printSomeMath(1.0)
    printSomeMath(two= 100.0)
    printSomeMath()

}