package com.av.kotlin

import java.math.BigDecimal
import kotlin.math.roundToInt

fun main() {
    val myDouble = 21.4

    println("Is myDouble a Double? => ${myDouble is Double}")
    println("what is myDouble? => ${myDouble::class.qualifiedName}")
    println("myDouble's java class => ${myDouble.javaClass}")

    val myDoubleToRoundedInt = myDouble.roundToInt()

    println("myDoubleToRoundedInt => $myDoubleToRoundedInt")

    val anotherInteger : Int = 17
    val myFloat : Float = 1.001F
    val myLong : Long = 1L
    val result = myFloat * myLong + anotherInteger
    println(result)

    val bd : BigDecimal = BigDecimal(17.99)
    val bd_2 : BigDecimal
    bd_2 = bd.add(BigDecimal(100000000))

    println(bd_2)


}