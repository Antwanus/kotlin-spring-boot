package com.av.kotlin

import kotlin.math.roundToInt
import kotlin.math.sqrt
import kotlin.reflect.full.functions

fun isPrime(x:Int):Boolean {
    val maxNumberToCheck = sqrt(x.toDouble()).roundToInt()
    println(maxNumberToCheck::class.qualifiedName)  // :: => reflection (like method references instead of a lambda)
    for (i in 2..maxNumberToCheck) if (x % i == 0) return false
    return true
}

fun main() {
    val myList = listOf(14,15,16,17,18,19,20)
//    val primeNumbers = myList.filter { isPrime(it) }
    val primeNumbers = myList.filter(::isPrime)
    // rather then passing an instance of a function, we're passing in a pointer to a function

    println(primeNumbers)

    val functionsOfClassOfObjectMyList = myList::class.functions
    functionsOfClassOfObjectMyList.forEach(System.out::println)
}