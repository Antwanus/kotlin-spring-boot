package com.av.kotlin

class MyCustomerClass(val name:String, var age:Int, val address:String = "") {
    var isApproved:Boolean = false
    set(value){
        if (age >=18) {
            field = value
        } else {
            println("ERROR: setting customer.isApproved($field) to $value is not allowed    ==>     { $age } < 18")
        }
    }
    val nextAge
    get() = age +1

    fun uppercaseName() = name.uppercase()

    override fun toString() = "$name $age $isApproved"

/*  JAVA_EQUIVALENT
    public static getInstance() {
        return MyCustomerClass("name", 0, "nowhere");
    }
*/
    companion object {
        // any functions here are static
        fun getInstance() = MyCustomerClass("staticGetInstance()", 0, "")
    }

}

fun main() {
    val mcc = MyCustomerClass("hopla", 12, "you know me")
    val mcc21 = MyCustomerClass("hopla", 21, "you know me")
    mcc.isApproved = true
    mcc21.isApproved = true
    println("${mcc.name}, aged ${mcc.age}, loc ${mcc.address}, isApproved = ${mcc.isApproved}," +
            " next year = ${mcc.nextAge}")
    println("${mcc21.uppercaseName()}, aged ${mcc21.age}, loc ${mcc21.address}, isApproved = ${mcc21.isApproved}," +
            " next year = ${mcc21.nextAge}")

    println(MyCustomerClass.getInstance().toString())

}