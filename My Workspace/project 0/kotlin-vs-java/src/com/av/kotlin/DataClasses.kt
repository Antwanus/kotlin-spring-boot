package com.av.kotlin

/** by pre-appending 'data' to the class declaration, we get functions out of the box:
 *                                                              [ equals(), hashCode(), toString(), copy() ]
 */
data class MyCustomer(val name:String, val address:String, var age:Int) {
    constructor(name:String, age:Int):this(name, "unknown", age)
}

fun main() {
    val c = MyCustomer("Sally", 29)
    println(c)
    val c2 = c.copy(name = "Diane")
    println(c2)

    //long
    val cName = c.name
    val cAge = c.age
    val cAddress = c.address
    println("$cName, $cAddress, $cAge")

    //short
    val(name, address, age) = c
    println("$name, $address, $age")
    //ORDER MATTERS WHEN EXTRACTING TO VALUES -> SEE DEFAULT CONSTRUCTOR
    val(wrongAge, wrongName, wrongAddress) = c2
    println("$wrongName, $wrongAddress, $wrongAge")

}