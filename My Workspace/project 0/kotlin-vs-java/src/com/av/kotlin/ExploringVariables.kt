package com.av.kotlin

fun main() {
    // var, variable is mutable
    var name: String = "Kong"
    // val, value cannot be changed/mutated
    val surName = "King"

//    name =  " " + name
    name = " $name"

    val full = surName + name

    print("full = $full")
    print("\tsurName.length = ${surName.length}")
    println("\t$10, \$max10")

    val story = """It was
        |       a dark 
        |   and 
        |       a story time
    """.trimMargin("|")

    println(story)

    val replaceBefore = story.replaceBefore("dark", "a very very very very ")

    print("replaceBefore(dark):{\n$replaceBefore\n}\n")
    val replaceAfterLast = story.replaceAfterLast("a ", "gloomy time")

    print("replaceAfterLast(a ):{\n$replaceAfterLast\n}\n")

}