package com.av.kotlin

import java.math.BigDecimal
import java.util.*

fun main() {
//      System.out.println("Hello World");

//      System.out.println("Hello World");
    var result: Any
    val randomInt = Random().nextInt(3)

    result =    if (randomInt == 1) BigDecimal(30)
                else "howdy"

    println("result is currently => $result")

    result =    if (result is BigDecimal) {
                    // because we checked if result is BigDecimal -> treat result as a BigDecimal (no casting needed)
                    result.add(BigDecimal.valueOf(1000000))
                } else {
                    // put it in uppercase
                    val tmp = result as String
                    tmp.uppercase()
                }

    System.out.printf("result is currently => $result")
}