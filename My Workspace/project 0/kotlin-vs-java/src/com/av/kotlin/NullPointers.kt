package com.av.kotlin

fun main() {
    var name: String? = null

//    name = "hi"

    if (name != null) println(name.uppercase())

    println("$name".uppercase())

    println(name?.uppercase())      // name != null ? name.uppercase : null


    var address = null
    println(address is Nothing)     // returns null because Nothing can't be instantiated
//    address = "hi"                // does not compile -> Nothing can't have a value
    println(name!!.uppercase())     // throws nullPointerException

    var myInt = null
//    myInt = 5                     // does not compile

    var myInt2:Int? = null
    myInt2 = 5

}