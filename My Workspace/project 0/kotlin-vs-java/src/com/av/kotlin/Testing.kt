package com.av.kotlin

import java.util.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.lang.IllegalArgumentException
import kotlin.test.*

class AgeCalculation {

    fun getAge(bDay:Calendar):Int {
        val today = Calendar.getInstance()
        if (bDay.timeInMillis > today.timeInMillis) throw IllegalArgumentException()

        val years = today.get(Calendar.YEAR) - bDay.get(Calendar.YEAR)
        return  if (bDay.get(Calendar.DAY_OF_YEAR) > today.get(Calendar.DAY_OF_YEAR)) {
                    years -1
                } else {
                    years
                }
    }

}

class AgeCalculationTests {
    private val ageCalculationObject = AgeCalculation()

    @Test
    fun checkAgeWhenBornTodayReturns0(){
        assertEquals(0, ageCalculationObject.getAge(Calendar.getInstance()))
    }
    @Test
    fun checkAgeWhenBorn1000DaysAgo(){
        val date = Calendar.getInstance()
        date.add(Calendar.DAY_OF_YEAR, -1000)
        println(date)
        assertEquals(2, ageCalculationObject.getAge(date))
    }
    @Test
    fun testForException(){
        val date = Calendar.getInstance()
        date.add(Calendar.DAY_OF_YEAR, 10)

        assertThrows<IllegalArgumentException> {
            ageCalculationObject.getAge(date)
        }
    }

}