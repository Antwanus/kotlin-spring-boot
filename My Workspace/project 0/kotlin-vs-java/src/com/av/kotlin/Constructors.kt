package com.av.kotlin
/*  class Customer {
        val name:String
        val address:String
        var age:Int

        constructor(name: String, address: String, age: Int ){
            this.name = name
            this.address = address
            this.age = age
    }
} */
/** Kotlin shorthand for default constructor */
class Customer(val name: String, val address: String, var age: Int) {
    constructor(name:String, age:Int) :this(name, "", age){
        println("secondary constructor")
    }
}
class AlternativeCustomer(val name:String, var age: Int) {
    var address:String = "body"     // var => able to set it later

    init {
        address = "initBlock"       // it seems init block overrides this
    }
//    this.address = "nope"              // != COMPILE

    constructor(name: String, address: String, age: Int): this(name, age) {
        this.address = address
    }
}
class AnotherAlternativeCustomer(val name:String, var age:Int, val address:String="DJ KHALID IN THE HOUSE")

fun main() {
    val c = Customer("hi", "here", 18)
    println("${c.javaClass}: ${c.name}, aged ${c.age}, lives at ${c.address}")
    println("----------------------------")
    val c2 = Customer("bye", 5)
    c2.age++
    println("${c2.javaClass}: ${c2.name}, aged ${c2.age}, lives at ${c2.address}")

    val ac = AlternativeCustomer("aa", 5)
    println("${ac.javaClass}: ${ac.name}, aged ${ac.age}, lives at ${ac.address}")

    val aac = AnotherAlternativeCustomer("aa", 5)
    println("${aac.javaClass}: ${aac.name}, aged ${aac.age}, lives at ${aac.address}")

}