package com.av.kotlin


import java.util.*

data class KotlinPerson(
        val id:Long,
        val title:String,
        val firstName:String,
        val lastName:String,
        val birthDay:Calendar?  //birthday is nullable
    ) {
    var favColour:String? = null

    fun getLastLetter(s:String) = s.takeLast(1)
    

    fun getUppercaseColour():String {
//        val tmp = favColour
//        return if (tmp == null) { "" } else { tmp.uppercase() }
        // favColour?.uppercase => expression: if(favColour!=null) return favColour.uppercase(); => if null return ""
        return favColour?.uppercase() ?: ""
    }

    fun getLastLetterOfColour():String {
        return favColour?.let { favC-> getLastLetter(favC) } ?: ""
        // if favColour is not null, use .let function { lambda } => if null return ""
    }

    fun getColorType():String {
        val colour = getUppercaseColour()
//        return  if (colour == "") {
//                    "empty"
//                } else if (colour == "RED" || colour == "GREEN" || colour == "BLUE"){
//                    "RGB"
//                } else {
//                    "other"
//                }
        return  when (colour) {
                    "" -> "empty"
                    "RED", "GREEN", "BLUE" -> "RGB"
                    else -> "other"
                }
        }

    val safeAge: Int
        get() = age ?: -1
        // { ?: }=>elvis-operator=> return (age != null) ? age : -1
    val age:Int?
        get() = getAge(birthDay)

    companion object {
        fun getAge(birthDay: Calendar?): Int? {
            if (birthDay == null) return null
            val today:Calendar = GregorianCalendar()
            val years:Int = today.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR)
            // if(expression) return true; else return false;
            return if (birthDay.get(Calendar.DAY_OF_YEAR) >= today.get(Calendar.YEAR)) years - 1 else years
        }
    }
    override fun toString(): String {
        return "$title - $firstName $lastName - $age"
    }
}

fun main() {
    val bDay1:Calendar = Calendar.getInstance()
    bDay1.set(1986, 11, 10)
    val antoon = KotlinPerson(1L, "Java Dev", "Antoon","Vereecken", bDay1)
    val bDay:Calendar = Calendar.getInstance()
    bDay.set(1989, 12, 5)
    val stefanie = KotlinPerson(2L, "baby", "Stefanie", "De Pestel", bDay)
    println(antoon)
    println(stefanie)

    println(antoon.age)
    println(KotlinPerson.getAge(GregorianCalendar(2000,1,1)))

    println("----------------------")
    val oldestPerson = if (antoon.safeAge > stefanie.safeAge) antoon else stefanie
    println("oldest person is ${oldestPerson.firstName}")
}