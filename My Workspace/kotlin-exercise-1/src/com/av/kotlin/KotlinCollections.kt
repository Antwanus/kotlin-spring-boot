package com.av.kotlin

fun main() {
    val listOfColours = listOf("RED", "GREEN", "BLUE")
    val filteredList = listOfColours.filter { s -> s == "RED" }
    println(filteredList)
    print("coloursList::class.qualifiedName =   ${listOfColours::class.qualifiedName}\n")
    // => most collections are immutable by default (they don't have a .put() for example)

    // for mutable collections, use mutableFoobarOf()
    mutableListOf("Monday", "Tuesday", "Wednesday")
    val mutableListOfInts = mutableListOf<Int>()
    mutableSetOf("Jan", "Feb")
    mutableMapOf("red" to "ff0000", "blue" to "00ff00")

    // arrays are mutable
    val intArray:IntArray = intArrayOf(1, 2, 3, 4, 5)
    intArray.set(3, -4)
    intArray[2] = -3

    println(listOfColours[0])
    intArray.forEach { println(it) }


}