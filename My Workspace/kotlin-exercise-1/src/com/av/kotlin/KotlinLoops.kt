package com.av.kotlin

import java.util.*
import kotlin.collections.ArrayList

fun main() {
    val bDay:Calendar = Calendar.getInstance()
    bDay.set(2021, 7, 7)

    val peopleMap = HashMap<Int, KotlinPerson>()
    peopleMap.put(1, KotlinPerson(1, "First", "James", "Bond", null))
    peopleMap.put(2, KotlinPerson(2, "Second", "Billy", "Bridger", null))
    peopleMap.put(3, KotlinPerson(3, "Third", "Donald", "Dump", null))
    peopleMap.put(4, KotlinPerson(4, "Fourth", "Elisabeth", "Vereecken", bDay))

    for ((key, value) in peopleMap) {
        println("Value($value) has Key($key)")
    }

    val peopleList = ArrayList<KotlinPerson>()
    peopleList.add(KotlinPerson(1, "First", "James", "Bond", null))
    peopleList.add(KotlinPerson(2, "Second", "Billy", "Bridger", null))
    peopleList.add(KotlinPerson(3, "Third", "Donald", "Dump", null))
    peopleList.add(KotlinPerson(4, "Fourth", "Elisabeth", "Vereecken", bDay))
    for (p:KotlinPerson in peopleList) {
        val (id, title) = p
        println("$id $title")
    }
    for ((id, title, firstName) in peopleList) {
        println("$firstName, $title ($id)")
    }

    val cLINE = "-------------------------------------------------"
    println(cLINE)

    val myRange = 0..9
    for (i in myRange) {
        print("$i... ")
    }
    println("\n$cLINE")

    for (i in 0..9) {
        print("________$i... ")
    }
    println("\n$cLINE")

    (0..9).forEach { i-> print("$i... ") }
    println("\n$cLINE")

    (9 downTo 0).forEach { print(it) }
    println("\n$cLINE")
    (0 until 9).forEach{ print(it) }
    println("\n$cLINE")
    (0..9 step 2).forEach { print(it) }
    println("\n$cLINE")

    ('A'..'Z').forEach { print(it) }
    println("\n$cLINE")


}